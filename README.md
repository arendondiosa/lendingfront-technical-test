# Lendingfront App

## Client

### Requirements/dependencies

* NodeJS >= 10.14.1

### Installation

Install modules/packages

```bash
cd src/client
npm install # yarn install
```

### Run

```bash
cd src/client/
yarn start
```

## API

### Requirements/dependencies

* Python >= 3.5

### Virtual Environment (Optional)

Create virtual environment

```bash
virtualenv env
```

Active virtual environment

```bash
# Linux
source env/bin/activate
# Windows
env/Scripts/activate
```

### Installation

Install modules/packages

```bash
cd src/api/
pip install -r requirements.txt
```

### Run

```bash
python main.py
```

### Test

```bash
tavern-ci test/test_minimal.tavern.yaml
```

### Routes

More information: [API Documentation](src/api/README.md)

## Run using Docker (Optional)

```bash
cd docker/
docker-compose up
```

## Example data

```
USER: admin@email.com
PASS:1234
```