// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
import vueResource from 'vue-resource'

import Index from './components/Index'
import Login from './components/Login'
import SignUp from './components/SignUp'
import Users from './components/Users'
import Businesses from './components/Businesses'
import NewBusiness from './components/NewBusiness'
import Decision from './components/Decision'

Vue.use(vueResource)
Vue.use(VueRouter)

function logout () {
  const user = localStorage.getItem('session')
  if (user) {
    localStorage.removeItem('session')
  }
  window.location.href = '/'
}

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    { path: '/', component: Index },
    { path: '/login', component: Login },
    { path: '/signup', component: SignUp },
    { path: '/users', component: Users },
    { path: '/business', component: Businesses },
    { path: '/business/new', component: NewBusiness },
    { path: '/decision', component: Decision },
    {
      path: '/logout',
      beforeEnter () {
        logout()
      }
    }
  ]
})

/* eslint-disable no-new */
new Vue({
  router,
  template: `
    <div id="app">
      <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
        <div class="mx-auto order-0">
          <a class="navbar-brand mx-auto" href="/">LendingFront App</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
              <span class="navbar-toggler-icon"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <router-link class="nav-link" to="/">Home</router-link >
            </li>
          </ul>
        </div>
        <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item"><router-link class="nav-link" to="/signup" v-if="!session">SignUp</router-link></li>
            <li class="nav-item"><router-link class="nav-link" to="/login" v-if="!session">Login</router-link></li>
            <li class="nav-item dropdown" v-if="session">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ activeUser }}
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <router-link class="dropdown-item" to="/business/new">Create Business</router-link>
                <router-link class="dropdown-item" to="/decision">Status</router-link>
                <router-link class="dropdown-item" to="/logout">Logout</router-link>
              </div>
            </li>
          </ul>
        </div>
      </nav>
      <div class="container" style="margin-top: 100px;">
        <router-view></router-view>
      </div>
    </div>
  `,
  data () {
    return {
      session: false,
      activeUser: ''
    }
  },
  methods: {
    activeSession () {
      const user = localStorage.getItem('session')
      if (user) {
        this.session = true
        this.activeUser = user
      }
    }
  },
  created: function () {
    this.activeSession()
  }
}).$mount('#app')
