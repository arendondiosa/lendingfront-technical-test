import tornado.httpserver
import tornado.escape
import tornado.ioloop
import tornado.web
import os
import bcrypt

users = {
    "123456": {
        "password": "$2b$10$weulyiEv.2XfBikMmoWFfuIEay0lMofK6thlpAdn.TV7FbIL4YlCm",
        "socialSecurityNumber": "123456",
        "name": "John",
        "lastName": "Roa",
        "email": "admin@email.com",
        "address": "Cra 6 # 6-6",
        "city": "Pereira",
        "state": "Risaralda",
        "postalCode": "660003"
    }
}

business = {
    "123abc": {
        "tax_id": "123abc",
        "name": "My Business",
        "address": "Cra 1 #1-1",
        "city": "Pereira",
        "state": "Risaralda",
        "postal_code": "660003",
        "request_amount": 1000000,
        "owner": "123456"
    }
}

# Checks if an object information is complete
def checkData(obj, size):
    for item in obj:
        if not obj[item]:
            return False
    
    if len(obj) == size:
        return True
    return False

# Valid User data from login form
def checkEmail(email):
    res_user = {}

    for user in users:
        query_res = users[user]
        if query_res["email"] == email:
            return users[user]
    return res_user

# Valid User data from Dicision view
def checkOwner(id):
    res_business = {}

    for item in business:
        query_res = business[item]
        if query_res["owner"] == id:
            return business[item]
    return res_business


class BaseHandler(tornado.web.RequestHandler):

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header("Access-Control-Allow-Methods",
                        " PUT, DELETE, OPTIONS")

    def options(self):
        # no body
        self.set_status(204)
        self.finish()


class Welcome(BaseHandler):
    def get(self):
        self.render("./api.html")

# Login as user using email and password
class LogIn(BaseHandler):
    def post(self):
        data = tornado.escape.json_decode(self.request.body)

        if data:
            email, password = data["email"], data["password"].encode("UTF_8")
            check_login = checkEmail(email)
            if check_login:
                user = check_login
                if bcrypt.checkpw(password, user["password"].encode("UTF_8")):
                    self.write({
                        "status": 200,
                        "message": "ok",
                        "data": user
                    })
                else:
                    self.write({
                        "status": 404,
                        "message": "wrong password",
                        "data": {}
                    })
            else:
                self.write({
                    "status": 404,
                    "message": "wrong information",
                    "data": {}
                })
        else:
            self.write({
                "status": 400,
                "message": "Need parameters to log in"
            })

# Creates a user account using Owner information
class SignUp(BaseHandler):
    def post(self):
        data = tornado.escape.json_decode(self.request.body)

        if data and checkData(data, 9):
            newUserId = data["socialSecurityNumber"]
            if newUserId in users:
                self.write({
                    "status": 500,
                    "message": "user exist"
                })
            else:
                passwordHashed = bcrypt.hashpw(data["password"].encode('UTF-8'), bcrypt.gensalt())
                users[newUserId] = data
                users[newUserId]["password"] = passwordHashed.decode()

                self.write({
                    "status": 200,
                    "message": "created user",
                    "data": users[newUserId]
                })
        else:
            self.write({
                "status": 400,
                "message": "need parameters to create a new user"
            })


# Returns all users registered
class Users(BaseHandler):
    def get(self):
        self.write({
            "status": 200,
            "message": "success",
            "data": users
        })


# Returns an user with specific email
class GetUser(BaseHandler):
    def post(self):
        data = tornado.escape.json_decode(self.request.body)
        if data:
            socialSecurityNumber = data["socialSecurityNumber"]
            if socialSecurityNumber in users:
                self.write({
                    "status": 200,
                    "message": "success",
                    "data": users[socialSecurityNumber]
                })
            else:
                self.write({
                    "status": 404,
                    "message": "user not found"
                })
        else:
            self.write({
                "status": 400,
                "message": "need parameters to search an user"
            })


# Returns an business with specific user Id
class GetBusinessByUser(BaseHandler):
    def post(self):
        data = tornado.escape.json_decode(self.request.body)

        if data:
            user_id = data["owner"]
            res_business = checkOwner(user_id)
            if res_business:
                self.write({
                    "status": 200,
                    "message": "success",
                    "data": res_business
                })
            else:
                self.write({
                    "status": 404,
                    "message": "business not found"
                })
        else:
            self.write({
                "status": 400,
                "message": "need parameters to search an user"
            })


# Returns all business registered
class Business(BaseHandler):
    def get(self):
        self.write({
            "status": 200,
            "message": "success",
            "data": business
        })

    def post(self):
        data = tornado.escape.json_decode(self.request.body)

        if data and checkData(data, 8):
            newBusinessId = data["tax_id"]
            if newBusinessId in business:
                self.write({
                    "status": 500,
                    "message": "business exist"
                })
            else:
                business[newBusinessId] = data

                self.write({
                    "status": 200,
                    "message": "created business",
                    "data": business[newBusinessId]
                })
        else:
            self.write({
                "status": 400,
                "message": "need parameters to create a new business"
            })



class SearchBusiness(BaseHandler):
    def get(self):
        data = tornado.escape.json_decode(self.request.body)

        if data:
            if data["tax_id"] in business:
                self.write({
                    "status": 200,
                    "message": "success",
                    "data": business[data["tax_id"]]
                })
            else:
                self.write({
                    "status": 404,
                    "message": "business not found"
                })
        else:
            self.write({
                "status": 400,
                "message": "need parameters to search a business"
            })


class SearchBusinessUsers(BaseHandler):
    def get(self):
        data = tornado.escape.json_decode(self.request.body)

        if data:
            if data["tax_id"] in business:
                self.write({
                    "status": 200,
                    "message": "success",
                    "data": business[data["tax_id"]]["owner"]
                })
            else:
                self.write({
                    "status": 404,
                    "message": "business not found"
                })
        else:
            self.write({
                "status": 400,
                "message": "need parameters to search a business"
            })


class LoanDecision(BaseHandler):
    def post(self):
        data = tornado.escape.json_decode(self.request.body)

        if data:
            request_amount = int(data["request_amount"])
            if request_amount > 50000:
                self.write({
                    "status": 200,
                    "message": "success",
                    "data": "declined"
                })
            elif request_amount == 50000:
                self.write({
                    "status": 200,
                    "message": "success",
                    "data": "undecided"
                })
            else:
                self.write({
                    "status": 200,
                    "message": "success",
                    "data": "approved"
                })
        else:
            self.write({
                "status": 400,
                "message": "need parameters to search a business"
            })


application = tornado.web.Application([
    (r"/api", Welcome),
    (r"/api/users", Users),
    (r"/api/user", GetUser),
    (r"/api/user/business", GetBusinessByUser),
    (r"/api/business", Business),
    (r"/api/business/search", SearchBusiness),
    (r"/api/business/search/users", SearchBusinessUsers),
    (r"/api/loan-decision", LoanDecision),
    (r"/api/login", LogIn),
    (r"/api/signup", SignUp)
])

if __name__ == "__main__":
    http_server = tornado.httpserver.HTTPServer(application)
    port = int(os.environ.get("PORT", 8888))
    http_server.listen(port)
    print("API Listen on port", port)
    tornado.ioloop.IOLoop.instance().start()
