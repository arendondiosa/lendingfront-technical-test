# App LendingFront API documentation version v.1.0
http://localhost:8888/api/

---

## /login
Login in LendingFront App

### /login

* **post**: Login as user using email and password

## /signup
Signup in LendingFront App

### /signup

* **post**: Creates a user account using Owner Information

## /users
People in LendingFront App

### /users

* **get**: Returns all users registered

## /user
Specific user in LendingFront App

### /user

* **post**: Returns an user with specific username

### /user/business
All business from a user

* **post**: Return all businesses from a user

## /business
Bussiness

### /business

* **get**: Return all Business
* **post**: Creates a new Business

### /business/search

* **get**: Return a project with specific id

### /business/search/users
Owner from a Business

* **get**: Return all Owners from business

## /loan-decision

### /loan-decision

* **post**: Return a decision from a Request Amount

